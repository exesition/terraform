# 1. Подготавливаем окружение

Обновляем версию terraform до 1.5.7. В моем случае потребовалось допонительно обновить утилиту yc для дальнейшей работы

<details>
<summary>Детали</summary>

Качаем пакет Terraform нужной версии и подсовываем бинарник в `/usr/bin/`<br>

Обновляем утилиту `yc`<br>
```bash
curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
```

Не забываем указать перед командой `terraform init` <br>
```bash
terraform providers lock -net-mirror=https://terraform-mirror.yandexcloud.net -platform=linux_amd64 -platform=darwin_arm64 yandex-cloud/yandex
```

</details>

В итоге получаем версию terraform 1.5.7 

<p align="left">
  <img src="./img/01_terver.png">
</p>

Создаём файл конфигурации Terraform CLI (~/.terraformrc):
```bash
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```

Далее создаём рабочую директорию и переходим в нее.
Создаём рабочее пространство (workspace):
```bash
terraform workspace new stage
Created and switched to workspace "stage"!

You're now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration.
```

Проверяем список workspace:
```bash
terraform workspace list
  default
* stage
```

При необходимости, переключаемся на нужный workspace:
```bash
terraform workspace select stage
Switched to workspace "stage".mc
```

# 2. Подготавливаем манифесты

Пoдготовил следующие файлы для разворачивания инфраструктуры:

- [node.tf](https://gitlab.com/exesition/terraform/-/blob/main/src/node.tf)
- [bucket.tf](https://gitlab.com/exesition/terraform/-/blob/main/src/bucket.tf)
- [networks.tf](https://gitlab.com/exesition/terraform/-/blob/main/src/networks.tf)
- [outputs.tf](https://gitlab.com/exesition/terraform/-/blob/main/src/outputs.tf)
- [providers.tf](https://gitlab.com/exesition/terraform/-/blob/main/src/providers.tf)
- [variables.tf](https://gitlab.com/exesition/terraform/-/blob/main/src/variables.tf)
- [versions.tf](https://gitlab.com/exesition/terraform/-/blob/main/src/versions.tf)

При первом запуске `terraform init`в файле `versions.tf` нужно закомменитрировать блок:
```text
#  backend "s3" {
#    endpoint   = "storage.yandexcloud.net"
#    bucket     = "bucket-diplom2024"
#    region     = "ru-central1-a"
#    key        = "terraform.tfstate.d/stage/terraform.tfstate"
#    access_key = "YC..H"
#    secret_key = "Y..U"

#    skip_region_validation      = true
#    skip_credentials_validation = true
#  }
```
Его мы добавим позже, когда создадим сам bucket.

Для получения и сохранения `access_key` и `secret_key` пропишем в файле `outputs.tf` следующее:

```text
output "access_key" {
  value = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  sensitive = true
}
output "secret_key" {
  value = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  sensitive = true
}
```
Это позволит сохранить значения `access_key` и `secret_key` в нашем локальном файле state и в дальнейшем использовать значения при подключение к `s3` в файле `versions.tf`


# 3. Инициализируем развертывание инфраструктуры


Выполняем первый запуск:
terraform init


```bash
Initializing the backend...


Initializing provider plugins...
- Finding yandex-cloud/yandex versions matching "0.121.0"...
- Installing yandex-cloud/yandex v0.121.0...
- Installed yandex-cloud/yandex v0.121.0 (unauthenticated)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

```

Проверяем конфигурацию:
```bash
terraform validate
Success! The configuration is valid.
```

После проверки конфигурации выполняем команду `terraform plan` и `terraform apply`

<details>
<summary>Листинг terraform plan</summary>

```bash
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # yandex_compute_instance.platform[0] will be created
  + resource "yandex_compute_instance" "platform" {
      + allow_stopping_for_update = true
      + created_at                = (known after apply)
      + folder_id                 = (known after apply)
      + fqdn                      = (known after apply)
      + gpu_cluster_id            = (known after apply)
      + hostname                  = "node-0"
      + id                        = (known after apply)
      + maintenance_grace_period  = (known after apply)
      + maintenance_policy        = (known after apply)
      + metadata                  = {
          + "serial-port-enable" = "1"
          + "ssh-keys"           = "ubuntu:ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII+NIpEX7M1OBxjRk9MKiwuSOc+1P2lfMvojrZ7MZSyh root@exe-ubuntu"
        }
      + name                      = "node-0"
      + network_acceleration_type = "standard"
      + platform_id               = "standard-v3"
      + service_account_id        = (known after apply)
      + status                    = (known after apply)
      + zone                      = "ru-central1-a"

      + boot_disk {
          + auto_delete = true
          + device_name = (known after apply)
          + disk_id     = (known after apply)
          + mode        = (known after apply)

          + initialize_params {
              + block_size  = (known after apply)
              + description = (known after apply)
              + image_id    = "fd8m2ak64lipvicd94sf"
              + name        = (known after apply)
              + size        = 20
              + snapshot_id = (known after apply)
              + type        = "network-hdd"
            }
        }

      + network_interface {
          + index              = (known after apply)
          + ip_address         = (known after apply)
          + ipv4               = true
          + ipv6               = (known after apply)
          + ipv6_address       = (known after apply)
          + mac_address        = (known after apply)
          + nat                = true
          + nat_ip_address     = (known after apply)
          + nat_ip_version     = (known after apply)
          + security_group_ids = (known after apply)
          + subnet_id          = (known after apply)
        }

      + resources {
          + core_fraction = 20
          + cores         = 4
          + memory        = 4
        }

      + scheduling_policy {
          + preemptible = true
        }
    }

  # yandex_compute_instance.platform[1] will be created
  + resource "yandex_compute_instance" "platform" {
      + allow_stopping_for_update = true
      + created_at                = (known after apply)
      + folder_id                 = (known after apply)
      + fqdn                      = (known after apply)
      + gpu_cluster_id            = (known after apply)
      + hostname                  = "node-1"
      + id                        = (known after apply)
      + maintenance_grace_period  = (known after apply)
      + maintenance_policy        = (known after apply)
      + metadata                  = {
          + "serial-port-enable" = "1"
          + "ssh-keys"           = "ubuntu:ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII+NIpEX7M1OBxjRk9MKiwuSOc+1P2lfMvojrZ7MZSyh root@exe-ubuntu"
        }
      + name                      = "node-1"
      + network_acceleration_type = "standard"
      + platform_id               = "standard-v3"
      + service_account_id        = (known after apply)
      + status                    = (known after apply)
      + zone                      = "ru-central1-b"

      + boot_disk {
          + auto_delete = true
          + device_name = (known after apply)
          + disk_id     = (known after apply)
          + mode        = (known after apply)

          + initialize_params {
              + block_size  = (known after apply)
              + description = (known after apply)
              + image_id    = "fd8m2ak64lipvicd94sf"
              + name        = (known after apply)
              + size        = 20
              + snapshot_id = (known after apply)
              + type        = "network-hdd"
            }
        }

      + network_interface {
          + index              = (known after apply)
          + ip_address         = (known after apply)
          + ipv4               = true
          + ipv6               = (known after apply)
          + ipv6_address       = (known after apply)
          + mac_address        = (known after apply)
          + nat                = true
          + nat_ip_address     = (known after apply)
          + nat_ip_version     = (known after apply)
          + security_group_ids = (known after apply)
          + subnet_id          = (known after apply)
        }

      + resources {
          + core_fraction = 20
          + cores         = 4
          + memory        = 4
        }

      + scheduling_policy {
          + preemptible = true
        }
    }

  # yandex_compute_instance.platform[2] will be created
  + resource "yandex_compute_instance" "platform" {
      + allow_stopping_for_update = true
      + created_at                = (known after apply)
      + folder_id                 = (known after apply)
      + fqdn                      = (known after apply)
      + gpu_cluster_id            = (known after apply)
      + hostname                  = "node-2"
      + id                        = (known after apply)
      + maintenance_grace_period  = (known after apply)
      + maintenance_policy        = (known after apply)
      + metadata                  = {
          + "serial-port-enable" = "1"
          + "ssh-keys"           = "ubuntu:ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII+NIpEX7M1OBxjRk9MKiwuSOc+1P2lfMvojrZ7MZSyh root@exe-ubuntu"
        }
      + name                      = "node-2"
      + network_acceleration_type = "standard"
      + platform_id               = "standard-v3"
      + service_account_id        = (known after apply)
      + status                    = (known after apply)
      + zone                      = "ru-central1-d"

      + boot_disk {
          + auto_delete = true
          + device_name = (known after apply)
          + disk_id     = (known after apply)
          + mode        = (known after apply)

          + initialize_params {
              + block_size  = (known after apply)
              + description = (known after apply)
              + image_id    = "fd8m2ak64lipvicd94sf"
              + name        = (known after apply)
              + size        = 20
              + snapshot_id = (known after apply)
              + type        = "network-hdd"
            }
        }

      + network_interface {
          + index              = (known after apply)
          + ip_address         = (known after apply)
          + ipv4               = true
          + ipv6               = (known after apply)
          + ipv6_address       = (known after apply)
          + mac_address        = (known after apply)
          + nat                = true
          + nat_ip_address     = (known after apply)
          + nat_ip_version     = (known after apply)
          + security_group_ids = (known after apply)
          + subnet_id          = (known after apply)
        }

      + resources {
          + core_fraction = 20
          + cores         = 4
          + memory        = 4
        }

      + scheduling_policy {
          + preemptible = true
        }
    }

  # yandex_iam_service_account.sa will be created
  + resource "yandex_iam_service_account" "sa" {
      + created_at = (known after apply)
      + folder_id  = (known after apply)
      + id         = (known after apply)
      + name       = "sa-for-bucket"
    }

  # yandex_iam_service_account_static_access_key.sa-static-key will be created
  + resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
      + access_key           = (known after apply)
      + created_at           = (known after apply)
      + description          = "static access key for object storage"
      + encrypted_secret_key = (known after apply)
      + id                   = (known after apply)
      + key_fingerprint      = (known after apply)
      + secret_key           = (sensitive value)
      + service_account_id   = (known after apply)
    }

  # yandex_resourcemanager_folder_iam_member.sa-editor will be created
  + resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
      + folder_id = "b1g...qa"
      + id        = (known after apply)
      + member    = (known after apply)
      + role      = "storage.editor"
    }

  # yandex_storage_bucket.test will be created
  + resource "yandex_storage_bucket" "test" {
      + access_key            = (known after apply)
      + acl                   = "public-read"
      + bucket                = "bucket-diplom2024"
      + bucket_domain_name    = (known after apply)
      + default_storage_class = (known after apply)
      + folder_id             = (known after apply)
      + force_destroy         = false
      + id                    = (known after apply)
      + secret_key            = (sensitive value)
      + website_domain        = (known after apply)
      + website_endpoint      = (known after apply)
    }

  # yandex_vpc_network.develop will be created
  + resource "yandex_vpc_network" "develop" {
      + created_at                = (known after apply)
      + default_security_group_id = (known after apply)
      + folder_id                 = (known after apply)
      + id                        = (known after apply)
      + labels                    = (known after apply)
      + name                      = "develop"
      + subnet_ids                = (known after apply)
    }

  # yandex_vpc_subnet.subnet_zones[0] will be created
  + resource "yandex_vpc_subnet" "subnet_zones" {
      + created_at     = (known after apply)
      + folder_id      = (known after apply)
      + id             = (known after apply)
      + labels         = (known after apply)
      + name           = "subnet-ru-central1-a"
      + network_id     = (known after apply)
      + v4_cidr_blocks = [
          + "10.10.1.0/24",
        ]
      + v6_cidr_blocks = (known after apply)
      + zone           = "ru-central1-a"
    }

  # yandex_vpc_subnet.subnet_zones[1] will be created
  + resource "yandex_vpc_subnet" "subnet_zones" {
      + created_at     = (known after apply)
      + folder_id      = (known after apply)
      + id             = (known after apply)
      + labels         = (known after apply)
      + name           = "subnet-ru-central1-b"
      + network_id     = (known after apply)
      + v4_cidr_blocks = [
          + "10.10.2.0/24",
        ]
      + v6_cidr_blocks = (known after apply)
      + zone           = "ru-central1-b"
    }

  # yandex_vpc_subnet.subnet_zones[2] will be created
  + resource "yandex_vpc_subnet" "subnet_zones" {
      + created_at     = (known after apply)
      + folder_id      = (known after apply)
      + id             = (known after apply)
      + labels         = (known after apply)
      + name           = "subnet-ru-central1-d"
      + network_id     = (known after apply)
      + v4_cidr_blocks = [
          + "10.10.3.0/24",
        ]
      + v6_cidr_blocks = (known after apply)
      + zone           = "ru-central1-d"
    }

Plan: 11 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + access_key  = (sensitive value)
  + cluster-k8s = [
      + (known after apply),
      + (known after apply),
      + (known after apply),
    ]
  + secret_key  = (sensitive value)
```
</details>

Чтобы создать ресурсы выполняем команду:

```bash
terraform apply
...
# Длинный вывод этапов создания ресурсов, в итоге:
...
yandex_compute_instance.platform[2]: Creation complete after 34s [id=fv4p6o3rqh9iuehlomkv]
yandex_compute_instance.platform[0]: Creation complete after 38s [id=fhmc0b1u11ssmev4dg3a]
yandex_compute_instance.platform[1]: Still creating... [40s elapsed]
yandex_compute_instance.platform[1]: Creation complete after 47s [id=epd3v9sv3h0u5o4qp8n6]

Apply complete! Resources: 7 added, 0 changed, 0 destroyed.

Outputs:

access_key = <sensitive>
cluster-k8s = [
  "name = node-0 | external-address=10.10.2.19 | internal-address=84.201.139.206",
  "name = node-1 | external-address=10.10.1.4 | internal-address=158.160.34.246",
  "name = node-2 | external-address=10.10.3.30 | internal-address=158.160.163.174",
]
secret_key = <sensitive>
```


## 4. Настройка миграции файла состояния на S3

В файле `versions.tf` прописываем:

```text
terraform {
  ...
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "bucket-diplom2024"
    region     = "ru-central1-a"
    key        = "terraform.tfstate.d/stage/terraform.tfstate"
    access_key = "YC..H"
    secret_key = "Y..U"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}
```

Затем выполняем `terraform init`.
Система снова инициализирует состояние текущего каталога и создаст файл state на удаленном хранилище.
Чтобы убедиться, можно зайти в наше хранилище, а в нем найти файл `terraform.tfstate.d/stage/terraform.tfstate`.

```bash
terraform init
Initializing the backend...
Do you want to migrate all workspaces to "s3"?
  Both the existing "local" backend and the newly configured "s3" backend
  support workspaces. When migrating between backends, Terraform will copy
  all workspaces (with the same names). THIS WILL OVERWRITE any conflicting
  states in the destination.
  
  Terraform initialization doesn't currently migrate only select workspaces.
  If you want to migrate a select number of workspaces, you must manually
  pull and push those states.
  
  If you answer "yes", Terraform will migrate all states. If you answer
  "no", Terraform will abort.

  Enter a value: y

Do you want to migrate all workspaces to "s3"?
  Both the existing "local" backend and the newly configured "s3" backend
  support workspaces. When migrating between backends, Terraform will copy
  all workspaces (with the same names). THIS WILL OVERWRITE any conflicting
  states in the destination.
  
  Terraform initialization doesn't currently migrate only select workspaces.
  If you want to migrate a select number of workspaces, you must manually
  pull and push those states.
  
  If you answer "yes", Terraform will migrate all states. If you answer
  "no", Terraform will abort.

  Enter a value: yes


Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Reusing previous version of yandex-cloud/yandex from the dependency lock file
- Using previously-installed yandex-cloud/yandex v0.121.0

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

```

Полученные IP адреса будем использовать для настройки и развёртки Kubernetes.

<p align="left">
  <img src="./img/01_03_terinstance.png">
</p>
<p align="left">
  <img src="./img/01_06_terbucket.png">
</p>
<p align="left">
  <img src="./img/01_02_tercloud.png">
</p>



## Проблемы

<details>
<summary>Проблемы и детали</summary>

Небольшая ремарка при использование зоны ru-central1-c упирался в квоту создания на создание инстансов. Написал в поддержку оказалось, что данная зона уже Deprecated

Ответ поддержки звучал так:

```
Здравствуйте!

Квоты, которые вы запросили, скорее всего не относятся к получаемому вами сообщению. В вашем манифесте указана зона ru-central1-c

Сейчас эта зона выводится из эксплуатации − https://yandex.cloud/ru/docs/overview/concepts/ru-central1-c-deprecation . Поэтому вероятнее всего, если вы измените ru-central1-c на ru-central1-d, ошибка уйдет.

Проверьте, пожалуйста.
```
</details>
<br>