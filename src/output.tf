
# Out IP
output "cluster-k8s" {
  value = [
    for i in yandex_compute_instance.platform :
    "name = ${i.name} | external-address=${i.network_interface.0.ip_address} | internal-address=${i.network_interface.0.nat_ip_address}"
  ]
}

output "access_key" {
  value     = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  sensitive = true
}
output "secret_key" {
  value     = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  sensitive = true
}
