

###cloud vars

## Переменная для token
variable "token" {
  type        = string
  description = "OAuth-token; https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token"
}

## Переменная для cloud_id
variable "cloud_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/cloud/get-id"
}

## Переменная для default_zone
variable "folder_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/folder/get-id"
}

## Переменная для default_zone
variable "subnet_zone" {
  type        = list(string)
  default     = ["ru-central1-a", "ru-central1-b", "ru-central1-d"]
  description = "https://cloud.yandex.ru/docs/overview/concepts/geo-scope"
}

variable "cidr" {
  type        = list(string)
  default     = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
  description = "https://cloud.yandex.ru/docs/overview/concepts/geo-scope"
}

## Переменная для vpc_name
variable "vpc_name" {
  type        = string
  default     = "develop"
  description = "VPC network&subnet name"
}

## Переменная для public_key
variable "public_key" {
  type    = string
  default = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII+NIpEX7M1OBxjRk9MKiwuSOc+1P2lfMvojrZ7MZSyh root@exe-ubuntu"
}

variable "ubuntu-2004-lts" {
  default = "fd8m2ak64lipvicd94sf"
}
